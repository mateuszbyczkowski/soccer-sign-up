package pl.soccer.signup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoccerSignUpApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoccerSignUpApplication.class, args);
	}
}
